# Bibliogram CSS banner

Experimental CSS replacement for the SVG banner of [Bibliogram.art](https://bibliogram.art)

![Firefox 77 render](https://gitlab.com/MrHeating/bibliogram-css-banner/-/raw/master/extras/Bibliogram_css.png)

<details markdown="1"><summary>Credit & tools</summary>

All credit goes to [TheFrenchGhosty](https://pussthecat.org) for the original art, [Cadence](https://cadence.moe) for the [SVG version](https://gitlab.com/MrHeating/bibliogram-css-banner/-/blob/master/extras/banner-min.svg) and [Google's Lobster font](https://fonts.google.com/specimen/Lobster).

Tools to create the "Cancer" font: [Webfont-generator](https://github.com/bdusell/webfont-generator) by [bdusell](https://github.com/bdusell) and [FontForge](https://fontforge.org) by [fontforge](https://github.com/fontforge).</details>

- [`a_Bibliogram_banner_showcase.html`](https://gitlab.com/MrHeating/bibliogram-css-banner/-/blob/master/a_Bibliogram_banner_showcase.html) is a demo
- [`Bibliogram_banner_raw.css`](https://gitlab.com/MrHeating/bibliogram-css-banner/-/blob/master/Bibliogram_banner_raw.css) is to be worked with
<details markdown='1'><summary>Experimental versions</summary>

- [`Bibliogram_banner_compact.css`](https://gitlab.com/MrHeating/bibliogram-css-banner/-/blob/master/Bibliogram_banner_compact.css) is an experimental css version made to be as compact as possible (276 bytes)
- [`Bibliogram_banner_standalone_direct_copy_paste.html`](https://gitlab.com/MrHeating/bibliogram-css-banner/-/blob/master/Bibliogram_banner_standalone_direct_copy_paste.html) is a quick'n'dirty copypasta based on [`Bibliogram_banner_compact.css`](https://gitlab.com/MrHeating/bibliogram-css-banner/-/blob/master/Bibliogram_banner_compact.css)
</details><details markdown="1"><summary>Pros (3)</summary>

- **saves Resources** <br>
The code & font totals to `(1.7 ~ 2 kB) + rendering` in RAM usage, opposed to  `8 kB` in RAM usage of the SVG banner<br> ![8 kB for the SVG](extras/svg_size.png)
- **sharp edges between the colors** (other than the first letter).
- Flexible: the text can be **easily changed** (or translated) to any language without an imaging tool
</details><details markdown="1"><summary>Cons (2)</summary>

- The first letter's **edge** between the colors **is not sharp** (especially when zoomed in). This is to achieve an anri-aliasing-like effect by using a (relatively) narrow gradient, because otherwise there's no greyscale.
- New letters would need a new font file.
</details>